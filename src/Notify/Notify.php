<?php

namespace BlueBrothers\Openapi\Notify;

use BlueBrothers\Openapi\EmptySignException;
use BlueBrothers\Openapi\NotifyException;
use BlueBrothers\Openapi\SignNotMatchException;
use BlueBrothers\Openapi\Util;


class Notify
{
    private $merchantId;
    private $secretKey;

    public function __construct($merchantId, $secretKey)
    {
        $this->merchantId = $merchantId;
        $this->secretKey  = $secretKey;
    }

    private function getSign($params)
    {
        return Util::getSign($this->secretKey, $params);
    }

    // verifySign 验证签名
    private function verifySign($req)
    {
        // 检查签名
        if (empty($req["sign"])) {
            throw new EmptySignException();
        }

        if ($this->getSign($req) != $req["sign"]) {
            throw new SignNotMatchException();
        }
    }


}