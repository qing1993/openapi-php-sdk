<?php

namespace BlueBrothers\Openapi;

use Exception;

class OpenapiException extends Exception
{
    public function __construct($msg)
    {
        parent::__construct($msg);
    }
}