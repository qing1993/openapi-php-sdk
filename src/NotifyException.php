<?php

namespace BlueBrothers\Openapi;

use Exception;

class NotifyException extends Exception
{
    public function __construct($msg)
    {
        parent::__construct($msg);
    }
}