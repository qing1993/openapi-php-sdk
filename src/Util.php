<?php

namespace BlueBrothers\Openapi;

class Util
{
    // GetDecodeCardMap 解密卡密为map
    // map的key为卡号，如果有密码，则value为密码，否则为空字符串
    public static function getDecodeCardMap($code, $secretKey) {
        $cardsText = self::getDecodeCardText($code, $secretKey);
        if ($cardsText === false) {
            throw new OpenapiException("没有卡密信息");
        }
        $cardSep = ",";
        $cardIdSep = ":";
        $data = explode($cardSep, $cardsText);
        $cards = array();
        foreach ($data as $item) {
            $card = explode($cardIdSep, $item);
            switch (count($card)) {
                case 2:
                    $cards[$card[0]] = $card[1];
                    break;
                default:
                    $cards[$item] = "";
                    break;
            }
        }
        return $cards;
    }

    // GetDecodeCardText 解密卡密为字符串
    public static function getDecodeCardText($code, $secretKey) {
        if ($code === "") {
            throw new OpenapiException("没有卡密信息");
        }

        // 第一步：base64解码
        $cipherText = base64_decode($code, true);

        if ($cipherText === false) {
            throw new OpenapiException("卡密不是标准base64编码");
        }

        // 第二步：aes-256-ecb 解密
        $decryptedCode = openssl_decrypt($cipherText,'AES-256-ECB', $secretKey, OPENSSL_RAW_DATA);

        if ($decryptedCode === false) {
            throw new OpenapiException("卡密解密失败");
        }
        // 第三步：去除两边的空白字符
        return trim($decryptedCode);;
    }

    // GetSign 获取签名
    public static function getSign($appSecret, $data ) {
        $keys = array_keys($data);
        sort($keys);
        $concatenatedData = "";
        $separator = "";
        foreach ($keys as $key) {
            if ($key == "sign" || $key == "extendParameter") {
                continue;
            }
            $concatenatedData .= $separator . $key . "=" . $data[$key];
            $separator = "&";
        }
        $concatenatedData .= "&key=" . $appSecret;
        $signature = md5($concatenatedData);
        return strtoupper($signature);
    }

}