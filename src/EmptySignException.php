<?php


namespace BlueBrothers\Openapi;

use Exception;

class EmptySignException extends Exception
{
    public function __construct()
    {
        parent::__construct("签名不能为空");
    }
}