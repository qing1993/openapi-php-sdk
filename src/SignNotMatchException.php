<?php


namespace BlueBrothers\Openapi;

use Exception;


class SignNotMatchException extends Exception
{
    public function __construct()
    {
        parent::__construct("签名错误");
    }
}