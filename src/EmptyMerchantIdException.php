<?php

namespace BlueBrothers\Openapi;

use Exception;

class EmptyMerchantIdException extends Exception
{
    public function __construct()
    {
        parent::__construct("merchantId 不能为空");
    }
}