<?php


namespace BlueBrothers\Openapi;

use Exception;

class EmptySecretKeyException extends Exception
{
    public function __construct()
    {
        parent::__construct("secretKey 不能为空");
    }
}
