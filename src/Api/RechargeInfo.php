<?php

namespace BlueBrothers\Openapi\Api;

use BlueBrothers\Openapi\OpenapiException;

trait RechargeInfo
{

    /**
     * RechargeInfo 余额查询接口
     * 接口说明：https://tvd8jq9lqkp.feishu.cn/wiki/ModNwquZWizj75kaz4ncksxNnqb
     * @return array 返回结果 {"code":"0000","balance":0.00}
     * @throws OpenapiException
     */
    public function RechargeInfo()
    {
        $response = $this->post(Constant::apiRechargeInfo, []);
        return json_decode($response, true);
    }



}
