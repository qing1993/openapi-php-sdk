<?php

namespace BlueBrothers\Openapi\Api;

use BlueBrothers\Openapi\OpenapiException;

trait CardIssue {
    /**
     * CardIssue 卡密重发短信接口
     * 接口说明：https://tvd8jq9lqkp.feishu.cn/wiki/JkSqwfASnirmjXkQiULc3Qt6nFH
     * @return array 返回结果 {"code":"2000","message":“”}
     * @throws OpenapiException
     */
    public function CardIssue($outTradeNo)
    {
        $values   = array(
            'outTradeNo' => $outTradeNo,// 合作商系统内部订单号,同一商户下不可重复
        );
        $response = $this->post(Constant::apiCardIssue, $values);
        return json_decode($response, true);
    }
}