<?php

namespace BlueBrothers\Openapi\Api;

use BlueBrothers\Openapi\OpenapiException;

trait RechargeProduct
{

    /**
     * RechargeProduct 获取商品列表接口
     * 接口说明：https://tvd8jq9lqkp.feishu.cn/wiki/XmAiwMQiZiVRxpkuVAgcz2jRnlb
     * @return array 返回结果 {"code":"0000","products":[{"product_id":"","channel_price":"","item_name":"","original_price":""}]}
     * @throws OpenapiException
     */
    public function RechargeProduct()
    {
        $response = $this->post(Constant::apiRechargeProduct, []);
        return json_decode($response, true);
    }
}
