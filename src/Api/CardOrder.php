<?php

namespace BlueBrothers\Openapi\Api;

use BlueBrothers\Openapi\OpenapiException;

trait CardOrder {
    /**
     * CardOrder 卡密下单接口
     * 接口说明：https://tvd8jq9lqkp.feishu.cn/wiki/Xe2owT9nFicNCdkmLJxcjarHnRh
     * @return array 返回结果 {"code":"2000","message":“”}
     * @throws OpenapiException
     */
    public function CardOrder($req)
    {
        $values = array(
            'outTradeNo'      => $req["outTradeNo"],// 合作商系统内部订单号,同一商户下不可重复， 同微信、支付宝的out_trade_no类似
            'productId'       => $req["productId"],// 商品编码
            'number'          => $req["number"],// 购买数量，目前只能固定是1
            'notifyUrl'       => $req["notifyUrl"],// 异步通知地址
        );

        if (!empty($req["mobile"])) {
            $values['mobile'] = $req["mobile"]; // 手机号，非必传
        }

        if (!empty($req["extendParameter"])) {
            $values['extendParameter'] = $req["extendParameter"]; // 扩展参数
        }

        $response = $this->post(Constant::apiCardOrder, $values);
        return json_decode($response, true);
    }
}