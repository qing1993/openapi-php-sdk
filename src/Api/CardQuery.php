<?php

namespace BlueBrothers\Openapi\Api;

use BlueBrothers\Openapi\OpenapiException;

trait CardQuery
{
    /**
     * CardQuery 获取卡密列表接口
     * 接口说明：https://tvd8jq9lqkp.feishu.cn/wiki/IU7twzg3Di9VRqknompctHuEnmc
     * @return array 返回结果 {"code":"0000","message":"","status":"","outTradeNo":"","cardCode":""}
     * @throws OpenapiException
     */
    public function CardQuery($outTradeNo)
    {
        $values   = array(
            'outTradeNo' => $outTradeNo,// 合作商系统内部订单号,同一商户下不可重复
        );
        $response = $this->post(Constant::apiCardQuery, $values);
        return json_decode($response, true);
    }

}
