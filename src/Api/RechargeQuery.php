<?php

namespace BlueBrothers\Openapi\Api;

use BlueBrothers\Openapi\OpenapiException;

trait RechargeQuery
{
    /**
     * RechargeProduct 获取商品列表接口
     * 接口说明：https://tvd8jq9lqkp.feishu.cn/wiki/PT46wpD0wiH3QvkYWU8csUtlncb
     * @return array 返回结果 {"code":"0000","message":"","status":"","outTradeNo":""}
     * @throws OpenapiException
     */
    public function RechargeQuery($outTradeNo)
    {
        $values   = array(
            'outTradeNo' => $outTradeNo,// 合作商系统内部订单号,同一商户下不可重复
        );
        $response = $this->post(Constant::apiRechargeQuery, $values);
        return json_decode($response, true);
    }

}
