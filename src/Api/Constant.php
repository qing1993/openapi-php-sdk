<?php

namespace BlueBrothers\Openapi\Api;
class Constant
{
    const baseUrl        = "https://openapi.1688sup.com";    // 正式 URL
    const baseUrlProd    = "https://openapi.1688sup.com";    // 正式 URL
    const baseUrlSandbox = "http://test.openapi.1688sup.cn"; // 沙箱 URL

    const apiRechargeOrder   = "/recharge/order";   // 直充下单接口
    const apiRechargeQuery   = "/recharge/query";   // 直充订单查询接口
    const apiRechargeProduct = "/recharge/product"; // 获取商品列表接口
    const apiRechargeInfo    = "/recharge/info";    // 余额查询接口
    const apiCardOrder       = "/card/order";       // 卡密下单接口
    const apiCardQuery       = "/card/query";       // 卡密订单查询接口
    const apiCardIssue       = "/card/issue";       // 卡密重发短信接口


    // 响应码，对于下单接口，只有2000表示成功，1005表示重复下单
    // 对于查询接口，只有0000表示成功，其他表示失败
    const CodeResponseSuccess    = "0000"; // 响应成功成功
    const CodeCreateOrderSuccess = "2000"; // 下单成功
    const CodeCreateOrderRepeat  = "1005"; // 下单重复

    const StatusCardIssueSuccess = "01"; // 重发卡密成功
    const StatusCardIssueFailed  = "02"; // 重发下发失败

    // 订单充值状态
    const OrderStatusSuccess    = "01"; // 充值成功
    const OrderStatusRecharging = "02"; // 充值中
    const OrderStatusFail       = "03"; // 充值失败
    const OrderStatusException  = "04"; // 充值异常，建议商户先调用人工处理

    // OrderIsSuccess 判断是否成功
    public static function OrderIsSuccess($code)
    {
        return $code == Constant::CodeCreateOrderSuccess;
    }

    // OrderIsRepeat 判断是否重复下单
    public static function OrderIsRepeat($code)
    {
        return $code == Constant::CodeCreateOrderRepeat;
    }


    // StatusIsSuccess 是否成功
    public static function StatusIsSuccess($status)
    {
        return $status == Constant::OrderStatusSuccess;
    }

    // StatusIsFailed 是否已失败
    // 注意！不是已经失败，不要直接当成功使用，因为还有可能是充值中，或充值异常需要人工确定处理
    public static function StatusIsFailed($status)
    {
        return $status == Constant::OrderStatusFail;
    }
}

