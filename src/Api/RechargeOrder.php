<?php

namespace BlueBrothers\Openapi\Api;

use BlueBrothers\Openapi\OpenapiException;

trait RechargeOrder
{


    /**
     * RechargeOrder 直充下单接口
     * 接口说明：https://tvd8jq9lqkp.feishu.cn/wiki/Kuj7wBHCWiT0SDkY6dMcXuuYn8f
     * @return array 返回结果 {"code":"2000","message":“”}
     * @throws OpenapiException
     */
    public function RechargeOrder($req)
    {
        $values = array(
            'outTradeNo'      => $req["outTradeNo"],// 合作商系统内部订单号,同一商户下不可重复， 同微信、支付宝的out_trade_no类似
            'productId'       => $req["productId"],// 商品编码
            'number'          => $req["number"],// 购买数量，目前只能固定是1
            'notifyUrl'       => $req["notifyUrl"],// 异步通知地址
            'rechargeAccount' => $req["rechargeAccount"],// 手机号，非必传
            'accountType'     => $req["accountType"],// 充值账号类型,1：手机号，2：QQ号，0：其他
        );

        if (!empty($req["extendParameter"])) {
            $values['extendParameter'] = $req["extendParameter"]; // 扩展参数
        }
        $response = $this->post(Constant::apiRechargeOrder, $values);
        return json_decode($response, true);
    }
}
