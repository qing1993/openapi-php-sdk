<?php

namespace BlueBrothers\Openapi\Api;

use BlueBrothers\Openapi\EmptyMerchantIdException;
use BlueBrothers\Openapi\EmptySecretKeyException;
use BlueBrothers\Openapi\OpenapiException;
use BlueBrothers\Openapi\Util;

class Client
{
    use RechargeInfo;
    use RechargeOrder;
    use RechargeQuery;
    use RechargeProduct;

    use CardQuery;
    use CardOrder;
    use CardIssue;

    private $merchantId;
    private $secretKey;
    private $isProd;
    private $httpClient;
    public function __construct($merchantId, $secretKey, $isProd, $timeout)
    {
        if (empty($merchantId)) {
            throw new EmptyMerchantIdException();
        }
        if (empty($secretKey)) {
            throw new EmptySecretKeyException();
        }
        $this->merchantId = $merchantId;
        $this->secretKey = $secretKey;
        $this->isProd = $isProd;
        $this->httpClient = new \GuzzleHttp\Client(['timeout' => $timeout]);
    }

    public function post($path, $values)
    {
        $values = $this->addCommonValues($values);
        $sign = $this->getSign($values);
        $values['sign'] = $sign;
        $reqBody = http_build_query($values);
        $reqUrl = $this->getUrl($path);

        try {
            $response = $this->httpClient->request('POST', $reqUrl, [
                'headers' => [
                    'Content-Type' => 'application/x-www-form-urlencoded',
                ],
                'body' => $reqBody,
            ]);
        } catch (\Exception $e) {
            throw new OpenapiException("Failed to send request to $reqUrl, parameters: $reqBody, error: " . $e->getMessage());
        }

        $statusCode = $response->getStatusCode();
        if ($statusCode !== 200) {
            throw new OpenapiException("Incorrect HTTP status code $statusCode for request $reqUrl");
        }

        $respBody = $response->getBody()->getContents();
        return $this->convertCode2String($respBody);
    }

    private function convertCode2String($data)
    {
        $decodedData = json_decode($data, true);
        if (isset($decodedData['code'])) {
            $code = $decodedData['code'];
            if (is_int($code) || is_float($code)) {
                $decodedData['code'] = strval($code);
                $data = json_encode($decodedData);
            }
        }
        return $data;
    }

    private function addCommonValues($values)
    {
        if (empty($values)) {
            $values = [];
        }
        $values['merchantId'] = $this->merchantId;
        $values['timeStamp'] = strval(time());
        return $values;
    }

    private function getUrl($path)
    {
        $baseUrl = $this->isProd ? Constant::baseUrl : Constant::baseUrlSandbox;
        return $baseUrl . $path;
    }

    private function getSign($params)
    {
        return Util::getSign($this->secretKey, $params);
    }
}
