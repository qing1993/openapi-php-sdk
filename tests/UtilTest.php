<?php

namespace BlueBrothers\Openapi\Tests;

use BlueBrothers\Openapi\Util;


class UtilTest extends TestCase
{

    public function testGetDecodeCardText()
    {
        $encCode   = 'ezvbQdCehTDO7M7g22FjThOCypEhk/kswonAbdEEVsY=';// 示例的密钥key
        $secretKey = '8db16e8cc8363ed4eb4c14f9520bcc32';

        try {
            //aes-256-ecb 解密
            $decryptedCode = Util::getDecodeCardText($encCode, $secretKey);
            $this->assertEquals("23123122123123d22231321", $decryptedCode);
        } catch (\Exception $e) {
            $this->fail("Exception should not be thrown");
        }
    }
}