<?php

namespace BlueBrothers\Openapi\Tests;

use BlueBrothers\Openapi\Api\Client;
use BlueBrothers\Openapi\Api\Constant;
use BlueBrothers\Openapi\Util;


class ClientTest extends TestCase
{
    const testMerchantId = "23329";
    const testSecretKey  = "8db16e8cc8363ed4eb4c14f9520bcc32";

    public function testRechargeProduct()
    {
        try {
            $client = new Client(self::testMerchantId, self::testSecretKey, false, 10);
            $resp   = $client->RechargeProduct();
            $this->assertEquals("0000", $resp["code"]);
            $this->assertArrayHasKey("products", $resp);
        } catch (\Exception $e) {
            $this->fail("Exception should not be thrown");
        }
    }

    public function testRechargeInfo()
    {
        try {
            $client = new Client(self::testMerchantId, self::testSecretKey, false, 10);
            $resp   = $client->RechargeInfo();
            $this->assertEquals("0000", $resp["code"]);
            $this->assertArrayHasKey("balance", $resp);
        } catch (\Exception $e) {
            $this->fail("Exception should not be thrown");
        }
    }

    public function testRechargeQuery()
    {
        try {
            $client = new Client(self::testMerchantId, self::testSecretKey, false, 10);
            $resp   = $client->RechargeQuery("2024010311453793968");
            $this->assertEquals("0000", $resp["code"]);
        } catch (\Exception $e) {
            $this->fail("Exception should not be thrown");
        }
    }

    public function testRechargeOrder()
    {
        try {
            $client = new Client(self::testMerchantId, self::testSecretKey, false, 10);
            $params = [
                'outTradeNo'      => sprintf("%s%s", date("YmdHis"), rand(10000, 99999)),// 合作商系统内部订单号,同一商户下不可重复， 同微信、支付宝的out_trade_no类似
                'productId'       => 101,// 商品编码
                'number'          => 1,// 购买数量，目前只能固定是1
                'notifyUrl'       => "http://127.0.0.1:8000/notify/test",// 异步通知地址
                'rechargeAccount' => "18000000000",// 手机号，非必传
                'accountType'     => 1,// 充值账号类型,1：手机号，2：QQ号，0：其他
            ];
            $resp   = $client->RechargeOrder($params);
            $this->assertArrayHasKey("code", $resp);

            if (Constant::OrderIsRepeat($resp["code"]) || Constant::OrderIsSuccess($resp["code"])) {

            } else {
                $this->fail("下单失败");
            }
        } catch (\Exception $e) {
            $this->fail("Exception should not be thrown");
        }
    }

    public function testCardIssue()
    {
        try {
            $client = new Client(self::testMerchantId, self::testSecretKey, false, 10);
            $resp   = $client->CardIssue("2024010311443735866");
            $this->assertArrayHasKey("code", $resp);
        } catch (\Exception $e) {
            $this->fail("Exception should not be thrown");
        }
    }

    public function testCardQuery()
    {
        try {
            $client = new Client(self::testMerchantId, self::testSecretKey, false, 10);
            $resp   = $client->CardQuery("2024010311443735866");
            $this->assertArrayHasKey("code", $resp);
        } catch (\Exception $e) {
            $this->fail("Exception should not be thrown");
        }
    }

    // CardOrder
    public function testCardOrder()
    {
        try {
            $client = new Client(self::testMerchantId, self::testSecretKey, false, 10);
            $params = [
                'outTradeNo'  => sprintf("%s%s", date("YmdHis"), rand(10000, 99999)),// 合作商系统内部订单号,同一商户下不可重复， 同微信、支付宝的out_trade_no类似
                'productId'   => 253,// 商品编码
                'number'      => 1,// 购买数量，目前只能固定是1
                'notifyUrl'   => "http://127.0.0.1:8000/notify/test",// 异步通知地址
                'mobile'      => "18000000000",// 手机号，非必传
                'accountType' => 1,// 充值账号类型,1：手机号，2：QQ号，0：其他
            ];
            $resp   = $client->CardOrder($params);
            // 下单成功
            $this->assertArrayHasKey("code", $resp);

            if (Constant::OrderIsRepeat($resp["code"]) || Constant::OrderIsSuccess($resp["code"])) {

            } else {
                $this->fail("下单失败");
            }
        } catch (\Exception $e) {
            $this->fail("Exception should not be thrown");
        }
    }

    public function testCheckSign()
    {
        try {
            // $_POST 可接受参数
            $params = [
                'outTradeNo'      => '41858615686074369',
                'merchantId'      => 23329,
                'rechargeAccount' => 1,
                'status'          => '01',
                'sign'            => '22A6AD70DBA0CEDC7A5CF1B8F907EB08',
            ];
            $newSign   = Util::getSign(self::testSecretKey, $params);
            $this->assertEquals($params['sign'], $newSign);

            // 成功回调
            // Constant::StatusIsSuccess($params["status"])
            $this->assertArrayHasKey("status", $params);
            $this->assertEquals(true, Constant::StatusIsSuccess($params["status"]));

            // 失败回调
            // Constant::StatusIsFail($params["status"])
        } catch (\Exception $e) {
            $this->fail("Exception should not be thrown");
        }
    }

    // getDecodeCardText
    public function testGetDecodeCardText()
    {
        try
        {
            $code = 'ezvbQdCehTDO7M7g22FjThOCypEhk/kswonAbdEEVsY=';
            $cardStr = Util::getDecodeCardText($code, self::testSecretKey);
            $this->assertEquals("23123122123123d22231321", $cardStr);
        } catch (\Exception $e) {
            $this->fail("Exception should not be thrown");
        }
    }
}