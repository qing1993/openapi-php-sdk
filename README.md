使用Composer安装
```
composer require chengdu-blue-brothers/openapi-php-sdk
```


# api 接口调用

```php
use BlueBrothers\Openapi\Api\Client;

const testMerchantId = "23329";
const testSecretKey  = "8db16e8cc8363ed4eb4c14f9520bcc32";
const isProd = false; // 是否是生产环境
try {
    $client = new Client(testMerchantId, testSecretKey, isProd, 10);
    $res = $client->RechargeProduct();
} catch (\Exception $e) {
    echo $e->getMessage();
}
```

# 解密卡密

```php
use BlueBrothers\Openapi\Util;

$encCode = 'ezvbQdCehTDO7M7g22FjThOCypEhk/kswonAbdEEVsY=';
// 示例的密钥key
$secretKey = '8db16e8cc8363ed4eb4c14f9520bcc32';

// 第二步：aes-256-ecb 解密
try {
    $decryptedCode = Util::getDecodeCardMap($encCode, $secretKey)  ;
} catch (\Exception $e) {
    echo $e->getMessage();
}
```